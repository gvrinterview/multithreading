﻿var numbers = new[] { 1, 0, 2, 5, 7, 9, 3, 2, 0, 0, 4, 3, 5, 1, 0 };

Console.WriteLine("Hello, World!");
Console.WriteLine("This program will sum lots of numbers using lots of threads!");
Console.WriteLine("The single-thread sum is: {0}", numbers.Sum());

var adder = new MultiThreadAdder(numbers);

Console.WriteLine("The multi-thread sum is: {0}", adder.GetSum());

public class MultiThreadAdder
{
    private readonly Queue<int> _queue;
    private readonly List<(Thread, ThreadState)> _threads = new();
    public MultiThreadAdder(IEnumerable<int> numbers)
    {
        _queue = new Queue<int>(numbers);
    }

    public int GetSum()
    {
        // use as many threads as processor cores
        var threadCount = Environment.ProcessorCount;
        
        Console.WriteLine($"Using {threadCount} threads");
        
        for (var i = 0; i < threadCount; i++)
        {
            var state = new ThreadState(_queue);
            var thread = new Thread(AddNumbersProc);
            thread.Start(state);
            _threads.Add((thread, state));
        }
        
        return _threads.Select(x => x.Item2.Accumulator).Sum();
    }

    private void AddNumbersProc(object? state)
    {
        if (state is ThreadState threadState)
        {
            while(threadState.Queue.Count > 0)
            {
                threadState.Accumulator += threadState.Queue.Dequeue();
            }
        }
    }
}

public class ThreadState
{
    public readonly Queue<int> Queue;
    public int Accumulator;

    public ThreadState(Queue<int> queue)
    {
        Queue = queue;
    }
}

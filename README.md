# Distress call
I have this magnificent multi-threader number adder but is a little buggy, can you help me solve the issues?

Sometimes is brilliantly correct

```
PS C:\Code\exercises\Multithreading\bin\Release\net6.0> .\Multithreading.exe
Hello, World!
This program will sum lots of numbers using lots of threads!
The single-thread sum is: 42
Using 4 threads
The multi-thread sum is: 42
```

but sometimes instead it raises an exception:

```
Unhandled exception. Unhandled exception. System.InvalidOperationException: Queue empty.
at System.Collections.Generic.Queue`1.ThrowForEmptyQueue()
at System.Collections.Generic.Queue`1.Dequeue()
at MultiThreadAdder.AddNumbersProc(Object state) in C:\Code\exercises\Multithreading\Program.cs:line 50
at System.Threading.Thread.StartCallback()
System.InvalidOperationException: Queue empty.
at System.Collections.Generic.Queue`1.ThrowForEmptyQueue()
at System.Collections.Generic.Queue`1.Dequeue()
at MultiThreadAdder.AddNumbersProc(Object state) in C:\Code\exercises\Multithreading\Program.cs:line 50
at System.Threading.Thread.StartCallback()
```

other times it even calculate the wrong numbers!

```
PS C:\Code\exercises\Multithreading\bin\Release\net6.0> .\Multithreading.exe
Hello, World!
This program will sum lots of numbers using lots of threads!
The single-thread sum is: 42
Using 4 threads
The multi-thread sum is: 44
```

What can I do?